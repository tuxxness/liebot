from builtins import object
import time
import unittest
import html
import re

from util import hook, timesince
lastlieid = {}

def db_init(db):
  "check to see that our db has the the seen table and return a connection."
  db.execute("create table if not exists lies(id, lie, nick, "
             "primary key(id))")
  db.commit()

def randomnick(db):
  randomnickname=db.execute("SELECT nick FROM nickcache ORDER BY RANDOM() LIMIT 1").fetchone()
  db.commit()
  return randomnickname[0]

def say_lie(lie, say=None, db=None):
  newlie=lie[0][0].replace("&39;", "'")
  
  for i in range(0, newlie.count("$nick")):
    newlie=re.sub('\$nick', randomnick(db), newlie, 1)

  say(html.unescape(newlie))
  return int(lie[0][1])

@hook.command
def addlie(inp, nick='', chan='', db=None, input=None, say=None):
  "!addlie <lie> -- Add a lie to the database. $nick will be replaced with a random nickname."
  if inp != "":
    lastid = db.execute("SELECT id FROM lies ORDER BY id DESC LIMIT 1").fetchone()
    if lastid[0] > 0:
      nextid=lastid[0]+1
      db.execute("INSERT INTO lies (id, lie, nick) VALUES (?, ?, ?)",(nextid,html.escape(inp),input.nick))
      say("Added lie.")
  db.commit()

@hook.command
def changelie(inp, nick='', chan='', db=None, input=None, say=None):
  "!changelie <lie> -- Change the last lie you added."
  if inp != "":
    lastid = db.execute("SELECT id,nick FROM lies WHERE nick=? ORDER BY id DESC LIMIT 1", (input.nick,)).fetchone()
    if lastid[0] > 0:
      db.execute("UPDATE lies set lie=? WHERE id=?",(inp, lastid[0]))
      say("Changed lie.")
  db.commit()

@hook.command(autohelp=False)
def who(inp, nick='', chan='', db=None, input=None, say=None):
  "!who -- Show who added the last displayed lie"
  global lastlieid
  try:
    who = db.execute("SELECT nick FROM lies WHERE id=? LIMIT 1", (lastlieid[input.chan],)).fetchone()
    db.commit()
    say(f"{who[0]}")
  except:
    say("No id found for last lie")

@hook.command('vs')
@hook.command
def versus(inp, nick='', chan='', db=None, input=None, say=None):
  "!versus <word1> <word2> -- Do a versus battle to see which word is used more in lies. (Will it be > 9000???)?"
  words=inp.split(" ")
  results={}
  i=1
  if len(words) is 2:
    for w in words:
      result = db.execute("SELECT COUNT(id) FROM lies WHERE lie LIKE ? LIMIT 1", ("%"+w+"%",)).fetchone()
      if result[0] != "":
        results[i]=result[0]
      else:
        results[i]=0
      i=i+1
    db.commit()

    if results[1] > results[2]:
      say(f"{words[0]}({results[1]}) has won from {words[1]}({results[2]})!")
    if results[1] < results[2]:
      say(f"{words[0]}({results[1]}) has lost from {words[1]}({results[2]})!")
    if results[1] == results[2]:
      say(f"{words[0]}({results[1]}) are tied {words[1]}({results[2]})!")


@hook.command('id', autohelp=False)
@hook.command(autohelp=False)
def lastid(inp, nick='', chan='', db=None, input=None, say=None):
  "!lastid -- Get the latest displayed lie id"
  global lastlieid
  try:
    lastid=lastlieid[input.chan]
    say(f"#{lastid}")
  except:
    say("None found.")

@hook.command(autohelp=False)
def lastlie(inp, nick='', chan='', db=None, input=None, say=None):
  "!lastlie -- Get the latest lie"
  global lastlieid
  lie = db.execute("SELECT lie,id FROM lies ORDER BY id DESC LIMIT 1").fetchall()
  lastlieid[input.chan] = say_lie(lie, say, db)

@hook.command(autohelp=False)
def lie(inp, nick='', chan='', db=None, input=None, say=None):
  global lastlieid
  "!lie <id | string> -- Display a lie"
  db_init(db)
  lie = None

  if inp != "":
    try:
      # LIE BY ID
      if int(inp) > 0:
        lie = db.execute("SELECT lie,id FROM lies WHERE id=? LIMIT 1", (int(inp),)).fetchall()
    except:
      pass

    if lie == None:
      # LIE BY STRING
      lie = db.execute("SELECT lie,id FROM lies WHERE lie LIKE ? ORDER BY RANDOM() LIMIT 1", ("%"+html.escape(inp)+"%",)).fetchall()
      if not lie:
        lie = None
  else:
    # RANDOM LIE
    lie = db.execute("SELECT lie,id FROM lies ORDER BY RANDOM() LIMIT 1").fetchall()

  db.commit()

  if lie != None:
    lastlieid[input.chan] = say_lie(lie, say, db)
  else:
    say("Lie not found.")
