from builtins import object
import time
import unittest

from util import hook, timesince


def db_init(db):
    "check to see that our db has the the seen table and return a connection."
    db.execute("CREATE TABLE IF NOT EXISTS nickcache(nick text not null)")
    db.commit()

@hook.singlethread
@hook.event('PRIVMSG', ignorebots=False)
def seeninput(paraml, input=None, db=None, bot=None):
    db_init(db)
    try:
      nickexists=db.execute("SELECT nick FROM nickcache WHERE nick=?", (input.nick,)).fetchone()[0]
    except:
      db.execute("insert or replace into nickcache(nick) values(?)", (input.nick,))
    db.commit()
