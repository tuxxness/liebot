from builtins import object
import time
import unittest
import re
import extraction, requests

from util import hook, timesince

@hook.command
def op(paraml, input=None, db=None, bot=None, say=None, conn=None):
  conn.cmd('MODE', [input.chan, "+o", input.nick])

@hook.event('JOIN', ignorebots=False)
def autoop(paraml, input=None, db=None, bot=None, say=None, conn=None):
  conn.cmd('MODE', [input.chan, "+o", input.nick])
