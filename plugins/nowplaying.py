from builtins import object
import time
import unittest
import pylast
import html
from datetime import datetime
import humanize

from util import hook, timesince


@hook.command('np')
@hook.command
def nowplaying(inp, nick='', chan='', db=None, input=None, say=None, bot=None):
  "!nowplaying <nick> -- Display last.fm last played song"
  LFM_API_KEY = bot.config.get('LFM_API_KEY', "")
  LFM_API_SECRET = bot.config.get('LFM_API_SECRET', "")
  LFM_USER = bot.config.get('LFM_USER', "")
  LFM_PASS = bot.config.get('LFM_PASS', "")
  password_hash = pylast.md5(LFM_PASS)
  network = pylast.LastFMNetwork(
              api_key = LFM_API_KEY, 
              api_secret = LFM_API_SECRET, 
              username = LFM_USER, 
              password_hash = password_hash
            )
  user = network.get_user(inp)
  np = user.get_now_playing()
  if np != None:
    np=html.unescape(str(np))
    say(f"{inp} is playing: {np}")
  else:
    recent_tracks=user.get_recent_tracks(1)
    last_song=recent_tracks[0][0]
    prettytime=humanize.naturaltime(
                datetime.now() - datetime.fromtimestamp(
                  float(recent_tracks[0][3])
                )
              )
    say(f"{inp} played: {last_song} - {prettytime}")
