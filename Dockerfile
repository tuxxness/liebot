FROM python:3.7-alpine
COPY . /app
RUN cd /app && \
    apk add --update --no-cache g++ gcc libxslt-dev libxml2-dev && \
    pip3 install -r requirements.txt
CMD python3 /app/bot.py
